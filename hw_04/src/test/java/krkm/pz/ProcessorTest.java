package krkm.pz;

import org.junit.Before;
import org.junit.Test;
import org.mockito.*;

import java.security.spec.RSAOtherPrimeInfo;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyString;

public class ProcessorTest {

    @Mock private Producer producer;
    @Mock private Consumer consumer;

    @Captor
    ArgumentCaptor<String> paramCaptor;

    @InjectMocks
    private Processor processor = new Processor();

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void processNormal() {
        // given
        String value = "test";
        Mockito.when(producer.produce())
                .thenReturn(value);
        // when
        processor.process();
        // then

        Mockito.verify(producer, Mockito.times(1))
                .produce();
        Mockito.verify(consumer, Mockito.times(1)).consume(paramCaptor.capture());
        String actualValue = paramCaptor.getValue();
        assertEquals(value, actualValue);
    }

    @Test(expected = IllegalStateException.class)
    public void processWithException() {
        // given
        Mockito.when(producer.produce())
                .thenReturn(null);
        // when
        processor.process();
        // then

    }

}