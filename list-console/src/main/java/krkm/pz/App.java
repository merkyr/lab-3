package krkm.pz;

/**
 * Hello world!
 *
 */

    public class App
    {
        static void demoList(ListQ list) throws CustomException {
            list.addToFront("TestOne");
            list.addToFront("TestTwo");
            list.addToBack("TestBackAdd");
            list.printQ();
        }


        public static void main( String[] args )
        {
            ListQ list = new ListQ();
            try {
                demoList(list);
            } catch (CustomException e) {
                System.out.println(e);
            }

        }
    }


