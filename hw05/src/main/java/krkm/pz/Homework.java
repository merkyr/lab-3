package krkm.pz;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.joining;

public class Homework {

    public static <T> Stream<T> zip(Stream<T> first, Stream<T> second) {
        Iterator<T> iterLeft = first.iterator();
        Iterator<T> iterRight = second.iterator();
        List<T> elements = new LinkedList<>();
        while (iterRight.hasNext() && iterLeft.hasNext()) {
            if (iterLeft.hasNext()){
                elements.add(iterLeft.next());
            }
            if (iterRight.hasNext()){
                elements.add(iterRight.next());
            }
        }
        return elements.stream();
    }

    public String getString(List<Integer> list) {
        return list.stream()
                .map(i -> i % 2 == 0 ? "e" + i : "o" + i)
                .collect(joining(","));
    }


    public static void main( String[] args )
    {
        Stream<Integer> FirstCollection = Stream.of(5, 6, 8);
        Stream<Integer> SecondCollection = Stream.of(11, 12);
        Stream<Integer> combinedStream = zip(FirstCollection, SecondCollection);
        combinedStream.forEach(System.out::println);
    }
}

