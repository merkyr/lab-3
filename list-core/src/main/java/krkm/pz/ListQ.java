package krkm.pz;

public class ListQ implements ListInter {

    private class Link {
        public String data;
        Link next;
    }

    private Link head;
    private Link tail;
    private int size;

    public int getSize() {
        return size;
    }

    public String get(int index) {
        // добавить валидацию индекса
        Link temp = head;
        int count=0;
        while (temp.next != null) {
            if (count == index) {
               return temp.data;
            }
            temp = temp.next;
            count++;
        }
        return null;
    }

    public void addToFront(String data) {
        Link elem = new Link();
        elem.data = data;
        if (isEmpty()) {
            head = elem;
            tail = elem;
        }

        elem.next = head;
        head = elem;
        size++;

    }

    public void addToBack(String data) {
        Link elem = new Link();
        elem.data = data;

        if (isEmpty()) {

            head = elem;
            tail = elem;
        }

        tail.next = elem;
        tail = elem;

        size++;
    }



    public void removeFromFront() throws CustomException {
        if (isEmpty()) {
            throw new CustomException("List is empty");
        }
        head = head.next;
        size--;

    }

    public void removeFromBack() throws CustomException {
        if (isEmpty()) {
            throw new CustomException("List is empty");
        }
        Link temp = head;
        Link t = new Link();

        while (temp.next != null) {
            t = temp;
            temp = temp.next;
        }
        tail = t;
        tail.next = null;
        size--;
    }


    private boolean isEmpty() {
        return (size == 0);
    }

    // вывод на экран
    public void printQ() throws CustomException {
        if (isEmpty()) {
            throw new CustomException("List is empty");
        }
        Link temp = head;
        while (temp != null) {
            System.out.println(temp.data);
            temp = temp.next;
        }
    }
}
