package krkm.pz;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class ListQTest {

    ListQ list;

    @Before
    public void setUp() throws Exception {
        list = new ListQ();
    }

    @Test
    public void addToFrontToEmptyList() {
        // given
        int expectedSize = 1;
        String expectedItem = "mystring";
        //when
        list.addToFront(expectedItem);
        int actualSize = list.getSize();
        String actualItem = list.get(0);
        // then
        assertEquals(expectedSize, actualSize);
        assertEquals(expectedItem, actualItem);
    }

    @Test
    public void addToFrontToNotEmptyList() {
        // given
        list.addToBack("Item1");
        list.addToBack("Item2");
        int expectedSize = 3;
        String expectedItem = "mystring";
        //when
        list.addToFront(expectedItem);
        int actualSize = list.getSize();
        String actualItem = list.get(0);
        // then
        assertEquals(expectedSize, actualSize);
        assertEquals(expectedItem, actualItem);
    }

    @Test
    public void addToBackToEmptyList() {
        // given
        int expectedSize = 1;
        String expectedItem = "mystring";
        //when
        list.addToBack(expectedItem);
        int actualSize = list.getSize();
        String actualItem = list.get(0);
        // then
        assertEquals(expectedSize, actualSize);
        assertEquals(expectedItem, actualItem);
    }

    @Test
    public void addToBackToNotEmptyList() {
        // given
        list.addToFront("Item1");
        list.addToFront("Item2");
        int expectedSize = 3;
        String expectedItem = "mystring";
        //when
        list.addToFront(expectedItem);
        int actualSize = list.getSize();
        String actualItem = list.get(0);
        // then
        assertEquals(expectedSize, actualSize);
        assertEquals(expectedItem, actualItem);
    }

    @Test
    public void removeFromFront() throws CustomException {
        // given
        list.addToFront("Item1");
        int expectedSize = 0;
        String expectedItem = "Item1";
        //when
        list.removeFromFront();
        int actualSize = list.getSize();
        String actualItem = list.get(0);
        // then
        assertEquals(expectedSize, actualSize);
        assertEquals(expectedItem, actualItem);
    }

    @Test
    public void removeFromBack() throws CustomException {
        // given
        list.addToBack("Item1");
        int expectedSize = 0;
        String expectedItem = "Item1";
        //when
        list.removeFromFront();
        int actualSize = list.getSize();
        String actualItem = list.get(0);
        // then
        assertEquals(expectedSize, actualSize);
        assertEquals(expectedItem, actualItem);
    }

}