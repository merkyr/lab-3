package krkm.pz;

public class CustomException extends Exception {

    public CustomException(String e) {
        super(e);
    }
}
