package krkm.pz;

public interface ListInter {
    int getSize ();
    String get(int index);
    void addToFront(String data);
    void addToBack(String data);
    void removeFromFront() throws CustomException;
    void removeFromBack() throws CustomException;
    void printQ() throws CustomException;
}
